import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'my-app';
  monArray = [];
  onSubmit(f: any){
    // @ts-ignore
    this.monArray.push(f.form.value);
    console.log(this.monArray);
  }
}
